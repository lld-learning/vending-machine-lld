package org.ashish.exceptions;

public class InvalidChoiceException extends RuntimeException{
    public InvalidChoiceException() {
    }

    public InvalidChoiceException(String message) {
        super(message);
    }
}
