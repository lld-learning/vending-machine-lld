package org.ashish.service;

import lombok.Getter;
import org.ashish.models.UserInput;
import org.ashish.models.VendingMachine;

import java.security.InvalidParameterException;

public class VendingMachineService {

    @Getter
    private final VendingMachine vendingMachine;

    public VendingMachineService(VendingMachine vendingMachine) {
        this.vendingMachine = vendingMachine;
    }

    public String insertCoin(Integer amount){
        return vendingMachine.insertCoin(amount);
    }

    public String chooseProduct(Integer rowNumber){
        return vendingMachine.chooseProduct(rowNumber);
    }

    public String dispenseProduct(){
        return vendingMachine.dispenseProduct();
    }
}
