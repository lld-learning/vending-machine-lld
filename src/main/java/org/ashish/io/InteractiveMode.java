package org.ashish.io;

import lombok.NonNull;
import org.ashish.commands.CommandExecutorFactory;
import org.ashish.commands.ExitCommandExecutor;
import org.ashish.models.Command;
import org.ashish.models.UserInput;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class InteractiveMode extends Mode{
    public InteractiveMode(@NonNull final CommandExecutorFactory commandExecutorFactory) {
        super(commandExecutorFactory);
    }

    @Override
    public void process() throws IOException {
        final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            final String input = reader.readLine();
            String[] inputArray=input.split(" ");
            String command=inputArray[0];

            final UserInput userInput = new UserInput(command, Arrays.copyOfRange(inputArray, 1, inputArray.length));
            this.executeCommand(userInput);
            if (userInput.getCommand().equals(ExitCommandExecutor.COMMAND_NAME)) {
                break;
            }
        }
    }


}
