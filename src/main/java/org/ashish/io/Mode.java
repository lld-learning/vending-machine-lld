package org.ashish.io;

import org.ashish.commands.CommandExecutor;
import org.ashish.commands.CommandExecutorFactory;
import org.ashish.models.UserInput;

import java.io.IOException;

public abstract class Mode {
    private final CommandExecutorFactory commandExecutorFactory;

    public Mode(CommandExecutorFactory commandExecutorFactory) {
        this.commandExecutorFactory = commandExecutorFactory;
    }

    protected String executeCommand(UserInput userInput){
        CommandExecutor commandExecutor=commandExecutorFactory.getCommandExecutor(userInput);
        commandExecutor.validateCommand(userInput);
        return commandExecutor.executeCommand(userInput);
    }

    public abstract void process() throws IOException;
}
