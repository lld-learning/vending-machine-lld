package org.ashish.states;

public interface State {
    String insertCoin(Integer amount);

    String chooseProduct(Integer rowNumber);
    String dispenseProduct();
}
