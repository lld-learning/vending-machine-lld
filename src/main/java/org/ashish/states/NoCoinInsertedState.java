package org.ashish.states;

import org.ashish.exceptions.InvalidChoiceException;
import org.ashish.models.Transaction;
import org.ashish.models.VendingMachine;

public class NoCoinInsertedState  implements State{
    private VendingMachine vendingMachine;

    public NoCoinInsertedState(VendingMachine vendingMachine) {
        this.vendingMachine = vendingMachine;
    }

    @Override
    public String insertCoin(Integer amount) {
        if(vendingMachine.getTransaction()==null)
            vendingMachine.setTransaction(Transaction.createTransaction(amount));
        else
            vendingMachine.getTransaction().addAmount(amount);
        vendingMachine.setCoinInsertedState();
        return "Coin Inserted";
    }

    @Override
    public String chooseProduct(Integer rowNumber) {
        throw new InvalidChoiceException("Please insert a coin first");
    }

    @Override
    public String dispenseProduct() {
        throw new InvalidChoiceException("Please insert a coin first");
    }
}
