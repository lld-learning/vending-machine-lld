package org.ashish.states;

import org.ashish.exceptions.InvalidChoiceException;
import org.ashish.models.Product;
import org.ashish.models.VendingMachine;

public class DispenseState  implements State{
    private VendingMachine vendingMachine;

    public DispenseState(VendingMachine vendingMachine) {
        this.vendingMachine = vendingMachine;
    }

    @Override
    public String insertCoin(Integer amount) {
        throw new InvalidChoiceException("Product dispensing. Please wait");
    }

    @Override
    public String chooseProduct(Integer rowNumber) {
        throw new InvalidChoiceException("Product dispensing. Please wait before selecting another product.");
    }

    @Override
    public String dispenseProduct() {

        if(vendingMachine.validateTransaction() &&
                vendingMachine.getInventory().isProductAvailable(vendingMachine.getTransaction().getProduct().getId())){
            vendingMachine.setDispenseState();
            Product product=vendingMachine.getTransaction().getProduct();
            vendingMachine.getInventory().deductProduct(product.getId());
            vendingMachine.setNoCoinInsertedState();
            return "Product "+product.getName()+" Dispensed";
        } else {
            return "Product selected is not available";
        }
    }
}
