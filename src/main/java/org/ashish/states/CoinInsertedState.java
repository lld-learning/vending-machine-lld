package org.ashish.states;

import org.ashish.exceptions.InvalidChoiceException;
import org.ashish.exceptions.InvalidStateException;
import org.ashish.exceptions.OutOfStockException;
import org.ashish.models.Product;
import org.ashish.models.Transaction;
import org.ashish.models.VendingMachine;

public class CoinInsertedState implements State{
    private VendingMachine vendingMachine;

    public CoinInsertedState(VendingMachine vendingMachine) {
        this.vendingMachine = vendingMachine;
    }

    @Override
    public String insertCoin(Integer amount) {
        if(vendingMachine.getTransaction()==null)
            throw new InvalidStateException();
        else
            vendingMachine.getTransaction().addAmount(amount);
        return "Coin Inserted";
    }

    @Override
    public String chooseProduct(Integer rowNumber) {
        if(!vendingMachine.getInventory().isAvailable(rowNumber))
            throw new OutOfStockException();
        Product product=vendingMachine.getInventory().getProductId(rowNumber);
        vendingMachine.getTransaction().setProduct(product);
        return "Product "+product.getName()+" selected";
    }

    @Override
    public String dispenseProduct() {
        if(vendingMachine.validateTransaction() &&
                vendingMachine.getInventory().isProductAvailable(vendingMachine.getTransaction().getProduct().getId())){
            vendingMachine.setDispenseState();
            Product product=vendingMachine.getTransaction().getProduct();
            vendingMachine.getInventory().deductProduct(product.getId());
            vendingMachine.setNoCoinInsertedState();
            return "Product "+product.getName()+" Dispensed";
        } else {
            return "Product selected is not available";
        }
    }
}
