package org.ashish.builder;

import org.ashish.models.VendingMachine;

public class VendingMachineBuilder {

    private VendingMachine vendingMachine;
    private int MAX_ROWS;
    private int MAX_SLOTS_IN_ROW;

    public VendingMachineBuilder() {

    }

    public VendingMachineBuilder withMaxRows(int maxRows){
        this.MAX_ROWS=maxRows;
        return this;
    }

    public VendingMachineBuilder withMaxSlotsinRows(int maxSlots){
        this.MAX_SLOTS_IN_ROW=maxSlots;
        return this;
    }
    public VendingMachine build(){
        return new VendingMachine(this.MAX_ROWS,this.MAX_SLOTS_IN_ROW);
    }
}
