package org.ashish.models;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class UserInput {
    private final String command;
    private final String[] params;
}
