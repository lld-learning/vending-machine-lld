package org.ashish.models;

import lombok.Getter;
import lombok.Setter;

@Getter
public class Transaction {

    private Integer amount;
    @Setter
    private Product product;

    public Transaction(Integer amount) {
        this.amount = amount;
    }

    public static Transaction createTransaction(Integer amount){
        Transaction transaction=new Transaction(amount);

        return transaction;
    }

    public void addAmount(Integer amount){
        this.amount+=amount;
    }
}
