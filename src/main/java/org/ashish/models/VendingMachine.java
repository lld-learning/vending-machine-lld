package org.ashish.models;

import lombok.Getter;
import lombok.Setter;
import org.ashish.exceptions.*;
import org.ashish.states.CoinInsertedState;
import org.ashish.states.DispenseState;
import org.ashish.states.NoCoinInsertedState;
import org.ashish.states.State;

public class VendingMachine {
    @Getter
    private final Inventory inventory;
    private final int MAX_ROWS;
    private final int MAX_SLOTS_IN_ROW;
    @Getter
    @Setter
    private Transaction transaction;

    private State currentState;
    private final CoinInsertedState coinInsertedState;
    private final NoCoinInsertedState noCoinInsertedState;
    private final DispenseState dispenseState;

    public VendingMachine(int MAX_ROWS, int maxSlotsInRow) {
        this.MAX_ROWS = MAX_ROWS;
        MAX_SLOTS_IN_ROW = maxSlotsInRow;
        this.inventory = new Inventory(MAX_ROWS, maxSlotsInRow);

        dispenseState = new DispenseState(this);
        coinInsertedState = new CoinInsertedState(this);
        noCoinInsertedState = new NoCoinInsertedState(this);
        this.currentState = noCoinInsertedState;
    }

    public String insertCoin(Integer amount){
        if(amount==null){
            throw new InvalidCoinException();
        }
        return this.currentState.insertCoin(amount);
    }

    public String chooseProduct(Integer rowNumber){
        if(rowNumber==null || rowNumber>MAX_ROWS){
            throw new InvalidChoiceException();
        }
        return this.currentState.chooseProduct(rowNumber);
    }

    public String dispenseProduct(){
        return this.currentState.dispenseProduct();
    }

    public boolean validateTransaction(){
        if(transaction==null)
            throw new InvalidStateException();
        if(transaction.getAmount()==null || transaction.getAmount()==0)
            throw new InsufficientMoneyException();
        if(transaction.getProduct()==null)
            throw new ProductNotSelectedException();
        if(transaction.getAmount()<transaction.getProduct().getPrice())
            throw new InsufficientMoneyException();
        return true;
    }

    public void setCoinInsertedState(){
        if(currentState!=noCoinInsertedState)
            throw new InvalidStateException();
        this.currentState=coinInsertedState;
    }
    public void setNoCoinInsertedState(){
        if(currentState!=dispenseState)
            throw new InvalidStateException();
        currentState=noCoinInsertedState;
    }

    public void setDispenseState(){
        if(currentState!=coinInsertedState)
            throw new InvalidStateException();
        currentState=dispenseState;
    }
}
