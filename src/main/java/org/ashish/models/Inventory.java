package org.ashish.models;

import org.ashish.exceptions.InventoryFullException;
import org.ashish.exceptions.ProductMismatchException;

import java.util.HashMap;
import java.util.Map;

public class Inventory {
    private final int MAX_ROWS;
    private final int MAX_SLOTS_IN_ROW;
    private Map<Integer,Product> rowProductMap=new HashMap<>();
    private Map<Integer,Integer> productQuantity=new HashMap<>();

    public Inventory(int maxSlots, int maxSlotsInRow) {
        MAX_ROWS = maxSlots;
        MAX_SLOTS_IN_ROW = maxSlotsInRow;
    }

    public String addProduct(Product product,int quantity, Integer row){
        int finalQuantity=productQuantity.getOrDefault(product.getId(),0)+quantity;
        if(finalQuantity>MAX_SLOTS_IN_ROW)
            throw new InventoryFullException();

        if(!rowProductMap.containsKey(row)){
            if(rowProductMap.size()==MAX_ROWS)
                throw new InventoryFullException();
            rowProductMap.put(row,product);
            productQuantity.put(product.getId(),0);
        } else {
            if(rowProductMap.get(row).getId()!=product.getId()){
                if(productQuantity.get(rowProductMap.get(row).getId())==0){
                    rowProductMap.put(row,product);
                    productQuantity.put(product.getId(),0);
                } else
                    throw new ProductMismatchException();
            }
        }

        productQuantity.put(product.getId(),productQuantity.get(product.getId())+quantity);
        return "Product Added";
    }

    public String deductProduct(Integer productId){
        productQuantity.put(productId,productQuantity.get(productId)-1);
        return "Product Removed";
    }
    public boolean isAvailable(Integer rowNumber){
        return productQuantity.getOrDefault(rowProductMap.get(rowNumber).getId(),0)>0;
    }
    public boolean isProductAvailable(Integer productId){
        return productQuantity.getOrDefault(productId,0)>0;
    }

    public Product getProductId(Integer rowNumber){
        return rowProductMap.get(rowNumber);
    }
}
