package org.ashish.commands;

import lombok.NonNull;
import org.ashish.exceptions.InvalidChoiceException;
import org.ashish.models.UserInput;

import org.ashish.service.VendingMachineService;

import java.util.HashMap;
import java.util.Map;

public class CommandExecutorFactory {
    private final Map<String,CommandExecutor> commandExecutorMap;


    public CommandExecutorFactory(@NonNull final VendingMachineService vendingMachineService) {
        this.commandExecutorMap = new HashMap<>();

        commandExecutorMap.put(ChooseProductCommandExecutor.COMMAND_NAME,new ChooseProductCommandExecutor(vendingMachineService));
        commandExecutorMap.put(DispenseCommandExecutor.COMMAND_NAME,new DispenseCommandExecutor(vendingMachineService));
    }

    public CommandExecutor getCommandExecutor(UserInput userInput){
        CommandExecutor commandExecutor=commandExecutorMap.get(userInput.getCommand());
        if(commandExecutor==null)
            throw new InvalidChoiceException();
        return commandExecutor;
    }
}
