package org.ashish.commands;

import lombok.NonNull;
import org.ashish.models.UserInput;
import org.ashish.service.VendingMachineService;

import java.security.InvalidParameterException;

public class InsertCoinCommandExecutor extends CommandExecutor{
    public static final String COMMAND_NAME="insert_coin";
    public InsertCoinCommandExecutor(@NonNull VendingMachineService vendingMachineService) {
        super(vendingMachineService);
    }

    @Override
    public boolean validateCommand(UserInput userInput) {
        try {
            Integer.valueOf(userInput.getParams()[0]);
        } catch (NumberFormatException n){
            throw new InvalidParameterException("Invalid Coin Inserted");
        }
        return true;
    }

    @Override
    public String executeCommand(UserInput userInput) {
        Integer coinAmount = Integer.valueOf(userInput.getParams()[0]);
        return this.vendingMachineService.insertCoin(coinAmount);
    }
}
