package org.ashish.commands;

import lombok.NonNull;
import org.ashish.models.UserInput;
import org.ashish.service.VendingMachineService;

public abstract class CommandExecutor {

    protected final VendingMachineService vendingMachineService;
    public CommandExecutor(@NonNull final VendingMachineService vendingMachineService) {
        this.vendingMachineService = vendingMachineService;
    }

    public abstract boolean validateCommand(UserInput userInput);

    public abstract String executeCommand(UserInput userInput);
}
