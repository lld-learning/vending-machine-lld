package org.ashish.commands;

import lombok.NonNull;
import org.ashish.models.UserInput;
import org.ashish.service.VendingMachineService;

public class DispenseCommandExecutor extends CommandExecutor{
    public static final String COMMAND_NAME="dispense";

    public DispenseCommandExecutor(@NonNull final VendingMachineService vendingMachineService) {
        super(vendingMachineService);
    }

    @Override
    public boolean validateCommand(UserInput userInput) {
        return true;
    }

    @Override
    public String executeCommand(UserInput userInput) {
        return vendingMachineService.dispenseProduct();
    }
}
