package org.ashish.commands;

import lombok.NonNull;
import org.ashish.models.UserInput;
import org.ashish.service.VendingMachineService;

import java.security.InvalidParameterException;

public class ChooseProductCommandExecutor extends CommandExecutor{
    public static final String COMMAND_NAME="choose_product";

    public ChooseProductCommandExecutor(@NonNull final VendingMachineService vendingMachineService) {
        super(vendingMachineService);
    }

    @Override
    public boolean validateCommand(UserInput userInput) {
        try {
            Integer.valueOf(userInput.getParams()[0]);
        } catch (NumberFormatException n){
            throw new InvalidParameterException("Invalid Product Chosen");
        }
        return true;
    }

    @Override
    public String executeCommand(UserInput userInput) {
        Integer rowNumber= Integer.valueOf(userInput.getParams()[0]);

        return this.vendingMachineService.chooseProduct(rowNumber);
    }
}
