package org.ashish.commands;

import lombok.NonNull;
import org.ashish.models.UserInput;
import org.ashish.service.VendingMachineService;

public class ExitCommandExecutor extends CommandExecutor{
    public static final String COMMAND_NAME="exit";
    public ExitCommandExecutor(@NonNull VendingMachineService vendingMachineService) {
        super(vendingMachineService);
    }

    @Override
    public boolean validateCommand(UserInput userInput) {
        return true;
    }

    @Override
    public String executeCommand(UserInput userInput) {
        return null;
    }
}
