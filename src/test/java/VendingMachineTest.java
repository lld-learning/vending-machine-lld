import org.ashish.builder.VendingMachineBuilder;
import org.ashish.commands.CommandExecutorFactory;
import org.ashish.io.InteractiveMode;
import org.ashish.io.Mode;
import org.ashish.models.Inventory;
import org.ashish.models.Product;
import org.ashish.models.VendingMachine;
import org.ashish.service.VendingMachineService;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;

import java.io.IOException;

public class VendingMachineTest {
    protected VendingMachineService vendingMachineService;
    protected VendingMachine vendingMachine;
    protected Inventory inventory;
    protected Mode mode;

    @Before
    public void setup(){
        vendingMachine=new VendingMachineBuilder()
                .withMaxRows(2)
                .withMaxSlotsinRows(2)
                .build();


        Product apple=new Product(1,"Apple",2);
        Product banana=new Product(2,"Banana",3);
        vendingMachine.getInventory().addProduct(apple,1,1);
        vendingMachine.getInventory().addProduct(banana,1,2);

        vendingMachineService=new VendingMachineService(vendingMachine);
        mode=new InteractiveMode(new CommandExecutorFactory(vendingMachineService));
    }

//    @Test
//    public void vendingMachineTest(){
//        System.out.println(vendingMachineService.insertCoin(1));
//
//        System.out.println(vendingMachineService.pressButton("product","1"));
//        System.out.println(vendingMachineService.insertCoin(2));
//        System.out.println(vendingMachineService.pressButton("dispense"));
//        System.out.println(vendingMachineService.pressButton("dispense"));
//        Assertions.assertTrue(!vendingMachine.getInventory().isAvailable(1));
//    }

    @Test
    public void vendingMachineTest() throws IOException {
        mode.process();
//        Assertions.assertTrue(!vendingMachine.getInventory().isAvailable(1));
    }
}
